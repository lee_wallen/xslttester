﻿using System.IO;

namespace XsltTester
{
    public class FileUtils
    {
        public static void SaveTextToFile(string saveToFile, string valueToSave)
        {
            using (var sw = new StreamWriter(saveToFile))
            {
                sw.Write(valueToSave);
            }
        }

        public static string ReadTextFromFile(string fileToRead)
        {
            string text;
            using (var sr = new StreamReader(fileToRead))
            {
                text = sr.ReadToEnd();
            }
            return text;
        }
    }
}