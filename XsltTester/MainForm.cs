﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Xsl;
using XsltTester;

namespace XslTester
{
    public partial class MainForm : Form
    {
        private const string XSL_FILTER = "XSL (*.xsl)|*.xsl";
        private const string XML_FILTER = "XML (*.xml)|*.xml";
        private string _previousText = string.Empty;

        public MainForm()
        {
            InitializeComponent();
        }

        private void OnTick(object sender, EventArgs e)
        {            
            var inputXml = txtXmlSource.Text;
            var transformXsl = txtXslSource.Text;

            if (!string.IsNullOrWhiteSpace(inputXml) && !string.IsNullOrWhiteSpace(transformXsl))
            {
                try
                {
                    var proc = new XslCompiledTransform();
                    using (var sr = new StringReader(transformXsl))
                    using (XmlReader xr = XmlReader.Create(sr))
                    {
                        proc.Load(xr);
                    }

                    using (var sr = new StringReader(inputXml))
                    using (var xr = XmlReader.Create(sr))
                    using (var sw = new StringWriter())
                    {
                        proc.Transform(xr, null, sw);
                        string resultXML = sw.ToString().Trim();

                        if (!resultXML.Equals(_previousText))
                        {
                            txtTransformation.Text = resultXML;
                            _previousText = resultXML;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (string.IsNullOrEmpty(txtTransformation.Text) || !txtTransformation.Text.Equals(_previousText))
                    {
                        txtTransformation.Text = ex.Message;
                        _previousText = txtTransformation.Text = string.Format("Problem with XML or XSL. Exception : {0}",
                            ex.GetBaseException().Message);   
                    }
                }
            }
            else
            {
                txtTransformation.Text = "";
            }
        }

        private void SaveXslMenuItemClick(object sender, EventArgs e)
        {
            SaveFile(XSL_FILTER, "Save XSL", txtXslSource.Text);
        }

        private void LoadFleIntoTextbox(string fileToLoad, RichTextBox textBox)
        {
            textBox.Text = FileUtils.ReadTextFromFile(fileToLoad);
        }

        private void OpenXslMenuItemClick(object sender, EventArgs e)
        {
            OpenSource(txtXslSource, XSL_FILTER);
        }

        private void OpenXmlMenuItemClick(object sender, EventArgs e)
        {
            OpenSource(txtXmlSource, XML_FILTER);
        }

        private void CloseMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void SaveOutputMenuItemClick(object sender, EventArgs e)
        {
            SaveFile(XML_FILTER, "Save Transformed XML", txtTransformation.Text);
        }

        private void SaveFile(string filter, string title, string valueToSave)
        {
            dlgSave.FileName = "";
            dlgSave.Title = title;
            dlgSave.Filter = filter;
            dlgSave.FilterIndex = 0;
            dlgSave.ShowDialog(this);

            if (!string.IsNullOrEmpty(dlgSave.FileName))
            {
                FileUtils.SaveTextToFile(dlgSave.FileName, valueToSave);
            }
        }

        private void OnLoad(object sender, EventArgs e)
        {
            timerValues.Text ="1000";
            timer.Interval = 1000;
            timer.Enabled = true;
            timer.Start();
        }

        private void timerValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (timerValues.Text.Equals("0"))
            {
                timer.Enabled = false;
                timer.Stop();
            }
            else
            {
                timer.Interval = Int32.Parse(timerValues.Text);
                if (timer.Enabled == false) timer.Enabled = true;
                timer.Start();
            }
        }

        private void btnSaveXsl_Click(object sender, EventArgs e)
        {
            SaveFile(XSL_FILTER, "Save XSL", txtXslSource.Text);
        }

        private void btnSaveTransformation_Click(object sender, EventArgs e)
        {
            SaveFile(XML_FILTER, "Save Transformed XML", txtTransformation.Text);
        }

        private void btnOpenXmlSource_Click(object sender, EventArgs e)
        {
            OpenSource(txtXmlSource, XML_FILTER);
        }

        private void OpenSource(RichTextBox textBox, string filter)
        {
            dlgOpen.FileName = "";
            dlgOpen.Filter = filter;
            dlgOpen.FilterIndex = 0;
            dlgOpen.ShowDialog(this);

            if (!string.IsNullOrEmpty(dlgOpen.FileName) && File.Exists(dlgOpen.FileName))
            {
                textBox.Text = FileUtils.ReadTextFromFile(dlgOpen.FileName);
            }
        }

        private void btnOpenXslSource_Click(object sender, EventArgs e)
        {
            OpenSource(txtXslSource, XSL_FILTER);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form aboutBox = new AboutBox();
            aboutBox.ShowDialog(this);
        }

        private void ChangeFontClick(object sender, EventArgs e)
        {
            ChangeFont();
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeFont();
        }

        private void ChangeFont()
        {
            dlgFont.Font = txtTransformation.Font;
            dlgFont.FontMustExist = true;
            dlgFont.ShowDialog(this);

            txtTransformation.Font = dlgFont.Font;
            txtXslSource.Font = dlgFont.Font;
            txtXmlSource.Font = dlgFont.Font;
        }

    }
}
